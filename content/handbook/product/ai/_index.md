---
title: AI-assisted features
description: "This page contains information about AI at GitLab."

---


This page serves as a resource for team members looking for information about working on AI features at GitLab. The first iteration of this page is a list of links, loosely organized. This will improve as we build up our best practices and consolidate/improve our documentation.


## General information about our AI features
* [List of Language Models](https://docs.gitlab.com/ee/user/ai_features.html#language-models): All the models we are using to power features.
* [GitLab Duo Features by Tier](https://docs.gitlab.com/ee/user/ai_features.html): All features that are available to use, and how to access them.
* [List of AI features](https://handbook.gitlab.com/handbook/engineering/development/data-science/#features): Current AI features in developement, the team that owns them and whether they are integrated into Duo Chat.
* [AI Ethics Principles for Product Development](/handbook/legal/ethics-compliance-program/ai-ethics-principles/#1-avoid-unfair-bias): Principles to guide our decision making as we continue to build AI features into GitLab and to ensure that these features properly embody our values.
* [Data usage and privacy](https://docs.gitlab.com/ee/user/ai_features.html#data-usage): Public facing docs about how we use and protect data. 


## Product and UX info for design and validation
* [Suggest an AI feature](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=AI%20Project%20Proposal&issue%5Btitle%5D=AI+Feature+Proposal:+): Issue template for suggesting or requesting an AI feature
* [UX Maturity Requirements](/handbook/product/ai/ux-maturity/#background): Supplements our Product Development Flow and Experiment/Beta/GA processes with guidelines specific to validating, testing and releases AI features.
* [Pajamas Guidelines](https://design.gitlab.com/usability/ai-human-interaction): How to design the user experience for AI features. Includes design patterns and guidelines.
* [UX Research in AI space](https://handbook.gitlab.com/handbook/product/ux/ux-research/research-in-the-ai-space/): Helps team members evaluate the usefullness and usability of AI features.

## Engineering
* [AI Continuity Plan](/handbook/product/ai/continuity-plan/): Process for selecting and maintaining AI vendors.
* [Data Science Stage](/handbook/engineering/development/data-science/)

## Support
* [AI Workflow](/handbook/support/workflows/ai_features/): This page has information for handling tickets related to AI features.



